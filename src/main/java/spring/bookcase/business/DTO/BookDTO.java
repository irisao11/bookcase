package spring.bookcase.business.DTO;

public class BookDTO {
    private String bookTitle;
    private int volumeNumber;
    private int pageNumber;
    private String author;
    private String section;
    private boolean borrowed;
    private String nameOfPersonWhoBorrowed;

    public BookDTO(){
    }

    public BookDTO(String bookTitle, int volumeNumber, int pageNumber, String author, String section, boolean borrowed, String nameOfPersonWhoBorrowed) {
        this.bookTitle = bookTitle;
        this.volumeNumber = volumeNumber;
        this.pageNumber = pageNumber;
        this.author = author;
        this.section = section;
        this.borrowed = borrowed;
        this.nameOfPersonWhoBorrowed = nameOfPersonWhoBorrowed;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public int getVolumeNumber() {
        return volumeNumber;
    }

    public void setVolumeNumber(int volumeNumber) {
        this.volumeNumber = volumeNumber;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public boolean isBorrowed() {
        return borrowed;
    }

    public void setBorrowed(boolean borrowed) {
        this.borrowed = borrowed;
    }

    public String getNameOfPersonWhoBorrowed() {
        return nameOfPersonWhoBorrowed;
    }

    public void setNameOfPersonWhoBorrowed(String nameOfPersonWhoBorrowed) {
        this.nameOfPersonWhoBorrowed = nameOfPersonWhoBorrowed;
    }

    @Override
    public String toString() {
        return "BookDTO{" +
                "bookTitle='" + bookTitle + '\'' +
                ", volumeNumber=" + volumeNumber +
                ", pageNumber=" + pageNumber +
                ", author='" + author + '\'' +
                ", section='" + section + '\'' +
                ", borrowed=" + borrowed +
                ", nameOfPersonWhoBorrowed='" + nameOfPersonWhoBorrowed + '\'' +
                '}';
    }
}
