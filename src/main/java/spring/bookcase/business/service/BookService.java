package spring.bookcase.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.bookcase.business.DTO.BookDTO;
import spring.bookcase.persistance.entities.Book;
import spring.bookcase.persistance.repository.BookDAO;

import java.util.LinkedList;
import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookDAO bookDAO;

    public void insertBook(BookDTO bookDTO) {
        Book book = new Book();
        book.setBookTitle(bookDTO.getBookTitle());
        book.setVolumeNumber(bookDTO.getVolumeNumber());
        book.setPageNumber(bookDTO.getPageNumber());
        book.setAuthor(bookDTO.getAuthor());
        book.setSection(bookDTO.getSection());
        book.setBorrowed(bookDTO.isBorrowed());
        book.setNameOfPersonWhoBorrowed(bookDTO.getNameOfPersonWhoBorrowed());
        bookDAO.insertBook(book);
    }

    public List<BookDTO> getBorrowedBooks() {
        List<BookDTO> listOfBorrowedBooksDTO = new LinkedList<>();
        List<Book> borrowedBooksList = bookDAO.getBorrowedBooks();

        for (int i = 0; i < borrowedBooksList.size(); i++) {
            BookDTO bookDTO = new BookDTO();
            bookDTO.setBookTitle(borrowedBooksList.get(i).getBookTitle());
            bookDTO.setAuthor(borrowedBooksList.get(i).getAuthor());
            bookDTO.setVolumeNumber(borrowedBooksList.get(i).getVolumeNumber());
            bookDTO.setPageNumber(borrowedBooksList.get(i).getPageNumber());
            bookDTO.setNameOfPersonWhoBorrowed(borrowedBooksList.get(i).getNameOfPersonWhoBorrowed());
            listOfBorrowedBooksDTO.add(bookDTO);
        }
        return listOfBorrowedBooksDTO;
    }

    public List<BookDTO> getUnborrowedBooks() {
        List<BookDTO> listOfUnborrowedBooksDTO = new LinkedList<>();
        List<Book> listOfUnborrowedBooks = bookDAO.getUnborrowedBooks();

        for (int i = 0; i < listOfUnborrowedBooks.size(); i++) {
            BookDTO bookDTO = new BookDTO();
            bookDTO.setBookTitle(listOfUnborrowedBooks.get(i).getBookTitle());
            bookDTO.setVolumeNumber(listOfUnborrowedBooks.get(i).getVolumeNumber());
            bookDTO.setPageNumber(listOfUnborrowedBooks.get(i).getPageNumber());
            bookDTO.setAuthor(listOfUnborrowedBooks.get(i).getAuthor());
            bookDTO.setSection(listOfUnborrowedBooks.get(i).getSection());
            bookDTO.setBorrowed(listOfUnborrowedBooks.get(i).isBorrowed());
            bookDTO.setNameOfPersonWhoBorrowed(listOfUnborrowedBooks.get(i).getNameOfPersonWhoBorrowed());
            listOfUnborrowedBooksDTO.add(bookDTO);
        }
        return listOfUnborrowedBooksDTO;
    }

    public List<BookDTO> getOneVolumeBooks() {
        List<BookDTO> oneVolumeBookListDTOS = new LinkedList<>();
        List<Book> oneVolumeList = bookDAO.getBooksOneVolume();

        for (int i = 0; i < oneVolumeList.size(); i++) {
            BookDTO bookDTO = new BookDTO();
            bookDTO.setBookTitle(oneVolumeList.get(i).getBookTitle());
            bookDTO.setVolumeNumber(oneVolumeList.get(i).getVolumeNumber());
            bookDTO.setAuthor(oneVolumeList.get(i).getAuthor());
            bookDTO.setPageNumber(oneVolumeList.get(i).getPageNumber());
            bookDTO.setSection(oneVolumeList.get(i).getSection());
            bookDTO.setBorrowed(oneVolumeList.get(i).isBorrowed());
            bookDTO.setNameOfPersonWhoBorrowed(oneVolumeList.get(i).getNameOfPersonWhoBorrowed());
            oneVolumeBookListDTOS.add(bookDTO);
        }
        return oneVolumeBookListDTOS;
    }

    public List<BookDTO> getBooksInSection(String section) {
        List<BookDTO> booksInThisSectionDTOS = new LinkedList<>();
        List<Book> booksInThisSection = bookDAO.getBooksInSection(section);

        for (Book b : booksInThisSection) {
            BookDTO bookDTO = new BookDTO();
            bookDTO.setBookTitle(b.getBookTitle());
            bookDTO.setAuthor(b.getAuthor());
            bookDTO.setSection(b.getSection());
            bookDTO.setVolumeNumber(b.getVolumeNumber());
            bookDTO.setPageNumber(b.getPageNumber());
            bookDTO.setBorrowed(b.isBorrowed());
            bookDTO.setNameOfPersonWhoBorrowed(b.getNameOfPersonWhoBorrowed());
            booksInThisSectionDTOS.add(bookDTO);
        }
        return booksInThisSectionDTOS;
    }

    public List<BookDTO> getBooksBorrowedTo(String name) {
        List<BookDTO> booksBorrowedToThisPersonDTO = new LinkedList<>();
        List<Book> booksBorrowedToThisPerson = bookDAO.getBooksBorrowedTo(name);

        for (Book b : booksBorrowedToThisPerson) {
            BookDTO bookDTO = new BookDTO();
            bookDTO.setBookTitle(b.getBookTitle());
            bookDTO.setAuthor(b.getAuthor());
            bookDTO.setSection(b.getSection());
            bookDTO.setVolumeNumber(b.getVolumeNumber());
            bookDTO.setPageNumber(b.getPageNumber());
            bookDTO.setBorrowed(b.isBorrowed());
            bookDTO.setNameOfPersonWhoBorrowed(b.getNameOfPersonWhoBorrowed());
            booksBorrowedToThisPersonDTO.add(bookDTO);
        }
        return booksBorrowedToThisPersonDTO;
    }

    public List<BookDTO> getUnborrowedBooksInThisSection(String section) {
        List<BookDTO> unborrowedBooksInSectionDTOS = new LinkedList<>();
        List<Book> unborrowedBooksInThisSection = bookDAO.getUnborrowedBooksFromSection(section);

        for (Book b : unborrowedBooksInThisSection) {
            BookDTO bookDTO = new BookDTO();
            bookDTO.setBookTitle(b.getBookTitle());
            bookDTO.setAuthor(b.getAuthor());
            bookDTO.setSection(b.getSection());
            bookDTO.setVolumeNumber(b.getVolumeNumber());
            bookDTO.setBorrowed(b.isBorrowed());
            bookDTO.setNameOfPersonWhoBorrowed(b.getNameOfPersonWhoBorrowed());
            unborrowedBooksInSectionDTOS.add(bookDTO);
        }
        return unborrowedBooksInSectionDTOS;
    }

    public List<BookDTO> getBorrowedBooksOfAuthor(String author) {
        List<BookDTO> borrowedBooksOfAuthorDTOS = new LinkedList<>();
        List<Book> borrowedBooksOfAuthor = bookDAO.getBorrowedBooksOfAuthor(author);

        for (Book b : borrowedBooksOfAuthor) {
            BookDTO bookDTO = new BookDTO();

            bookDTO.setBookTitle(b.getBookTitle());
            bookDTO.setAuthor(b.getAuthor());
            bookDTO.setSection(b.getSection());
            bookDTO.setBorrowed(b.isBorrowed());
            bookDTO.setNameOfPersonWhoBorrowed(b.getNameOfPersonWhoBorrowed());
            borrowedBooksOfAuthorDTOS.add(bookDTO);
        }
        return borrowedBooksOfAuthorDTOS;
    }

    public List<BookDTO> getOrderListAscendant() {
        List<BookDTO> ascentantListOfBooksDTO = new LinkedList<>();
        List<Book> ascentantListOfBooks = bookDAO.orderBooksAscendant();

        for (Book b : ascentantListOfBooks) {
            BookDTO bookDTO = new BookDTO();

            bookDTO.setBookTitle(b.getBookTitle());
            bookDTO.setAuthor(b.getAuthor());
            ascentantListOfBooksDTO.add(bookDTO);
        }
        return ascentantListOfBooksDTO;
    }

    public int deleteBooksByTitleAuthorVolume(String title, String author, int volume) {
        return bookDAO.deleteBookByTitleAuthorVolume(title, author, volume);
    }
}
