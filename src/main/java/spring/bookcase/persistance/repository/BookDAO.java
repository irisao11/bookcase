package spring.bookcase.persistance.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import spring.bookcase.persistance.config.HibernateUtil;
import spring.bookcase.persistance.entities.Book;

import javax.persistence.Query;
import java.util.List;

@Repository
public class BookDAO {
    private HibernateUtil hibernateUtil;

    public BookDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void insertBook(Book book) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.persist(book);
        transaction.commit();
        session.close();
    }

    public List<Book> getBorrowedBooks() {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query borrowedBooksQuery = session.createNamedQuery("find_books_borrowed");
        List<Book> borrowedBooks = borrowedBooksQuery.getResultList();
        transaction.commit();
        session.close();
        return borrowedBooks;
    }

    public List<Book> getUnborrowedBooks() {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query unborrowedBooksQuery = session.createNamedQuery("find_books_unborrowed");
        List<Book> unborrowedBooks = unborrowedBooksQuery.getResultList();
        transaction.commit();
        session.close();
        return unborrowedBooks;
    }

    public List<Book> getBooksOneVolume() {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query oneVolumeQuery = session.createNamedQuery("books_with_one_volume");
        List<Book> oneVolumeBooks = oneVolumeQuery.getResultList();
        transaction.commit();
        session.close();
        return oneVolumeBooks;
    }

    public List<Book> getBooksInSection(String section) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query booksInSectionQuery = session.createNamedQuery("find_books_in_category");
        booksInSectionQuery.setParameter("section", section);
        List<Book> booksInSection = booksInSectionQuery.getResultList();
        transaction.commit();
        session.close();
        return booksInSection;
    }

    public List<Book> getBooksBorrowedTo(String name) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query borrowedToQuery = session.createNamedQuery("all_books_borrowed_to");
        borrowedToQuery.setParameter("nameOfPersonWhoBorrowed", name);
        List<Book> booksBorrowedToList = borrowedToQuery.getResultList();
        transaction.commit();
        session.close();
        return booksBorrowedToList;
    }

    public List<Book> getUnborrowedBooksFromSection(String section) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query unborrowedBooksFromSectionQuery = session.createNamedQuery("books_in_section_and_not_borrowed");
        unborrowedBooksFromSectionQuery.setParameter("section", section);
        List<Book> unborrowedBooksFromSection = unborrowedBooksFromSectionQuery.getResultList();
        transaction.commit();
        session.close();
        return unborrowedBooksFromSection;
    }

    public List<Book> getBorrowedBooksOfAuthor(String author) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query borrowedBooksOfAuthorQuery = session.createNamedQuery("books_with_author_borrowed");
        borrowedBooksOfAuthorQuery.setParameter("author", author);
        List<Book> borrowedBooksOfAuthor = borrowedBooksOfAuthorQuery.getResultList();
        transaction.commit();
        session.close();
        return borrowedBooksOfAuthor;
    }

    public List<Book> orderBooksAscendant() {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query orderAscendantQuery = session.createNamedQuery("select_ascendant");
        List<Book> ascendingList = orderAscendantQuery.getResultList();
        transaction.commit();
        session.close();
        return ascendingList;
    }

    public int deleteBookByTitleAuthorVolume(String bookTitle, String author, int volume) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query deleteBookQuery = session.createNamedQuery("delete_book");
        deleteBookQuery.setParameter("bookTitle", bookTitle).setParameter("author", author).setParameter("volumeNumber", volume);
        int numOfRowsDeleted = deleteBookQuery.executeUpdate();
        transaction.commit();
        session.close();
        return numOfRowsDeleted;
    }

}
