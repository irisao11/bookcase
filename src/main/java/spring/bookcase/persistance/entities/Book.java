package spring.bookcase.persistance.entities;

import javax.persistence.*;
import java.util.Objects;

@NamedQueries(
        {@NamedQuery(name = "find_books_borrowed",
                query = "select b from Book b where b.borrowed = true"),
        @NamedQuery(name = "find_books_unborrowed",
                query = "select b from Book b where b.borrowed = false"),
        @NamedQuery(name = "books_with_one_volume",
                query = "select b from Book b where b.volumeNumber = 1"),
        @NamedQuery(name = "find_books_in_category",
                query = "select b from Book b where b.section = :section"),
        @NamedQuery(name = "all_books_borrowed_to",
                query = "select b from Book b where b.nameOfPersonWhoBorrowed = :nameOfPersonWhoBorrowed"),
        @NamedQuery(name = "books_in_section_and_not_borrowed",
                query = "select b from Book b where b.section = :section and b.borrowed = false"),
        @NamedQuery(name = "books_with_author_borrowed",
                query = "select b from Book b where b.author = :author and b.borrowed = true"),
        @NamedQuery(name = "select_ascendant",
                query = "select b from Book b Order by b.bookTitle asc"),
        @NamedQuery(name = "delete_book",
                query = "delete from Book b where b.bookTitle = :bookTitle and b.author = :author and b.volumeNumber = :volumeNumber")})

@Entity
@Table(name = "bookcase")
public class Book {

    @Id
    private int id;

    @Column(name = "book_title")
    private String bookTitle;

    @Column(name = "volume_number")
    private int volumeNumber;

    @Column(name = "page_number")
    private int pageNumber;

    @Column(name = "author")
    private String author;

    @Column(name = "section")
    private String section;

    @Column(name = "borrowed")
    private boolean borrowed;

    @Column(name = "borrowed_to")
    private String nameOfPersonWhoBorrowed;

    public Book() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public int getVolumeNumber() {
        return volumeNumber;
    }

    public void setVolumeNumber(int volumeNumber) {
        this.volumeNumber = volumeNumber;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public boolean isBorrowed() {
        return borrowed;
    }

    public void setBorrowed(boolean borrowed) {
        this.borrowed = borrowed;
    }

    public String getNameOfPersonWhoBorrowed() {
        return nameOfPersonWhoBorrowed;
    }

    public void setNameOfPersonWhoBorrowed(String nameOfPersonWhoBorrowed) {
        this.nameOfPersonWhoBorrowed = nameOfPersonWhoBorrowed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id &&
                volumeNumber == book.volumeNumber &&
                pageNumber == book.pageNumber &&
                borrowed == book.borrowed &&
                Objects.equals(bookTitle, book.bookTitle) &&
                Objects.equals(author, book.author) &&
                Objects.equals(section, book.section) &&
                Objects.equals(nameOfPersonWhoBorrowed, book.nameOfPersonWhoBorrowed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bookTitle, volumeNumber, pageNumber, author, section, borrowed, nameOfPersonWhoBorrowed);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookTitle='" + bookTitle + '\'' +
                ", volumeNumber=" + volumeNumber +
                ", pageNumber=" + pageNumber +
                ", author='" + author + '\'' +
                ", section='" + section + '\'' +
                ", borrowed=" + borrowed +
                ", borrowedTo='" + nameOfPersonWhoBorrowed + '\'' +
                '}';
    }
}
