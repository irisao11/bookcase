package spring.bookcase.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.bookcase.business.DTO.BookDTO;
import spring.bookcase.business.service.BookService;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/bookcase/")
public class
BookController {

    @Autowired
    BookService bookService;

    @GetMapping(path = "getBorrowedBooks")
    public List<BookDTO> getBorrowedList() {
        List<BookDTO> bookDTOS = new LinkedList<>();
        return bookDTOS = bookService.getBorrowedBooks();
    }

    @GetMapping(path = "getUnborrowedBooks")
    public List<BookDTO> getUnborrowedList() {
        List<BookDTO> unborrowedBooksDTO = new LinkedList<>();
        return unborrowedBooksDTO = bookService.getUnborrowedBooks();
    }

    @GetMapping(path = "getOneVolumeBooks")
    public List<BookDTO> getOneVolumeBooks() {
        List<BookDTO> oneVolumeDTOS = new LinkedList<>();
        return oneVolumeDTOS = bookService.getOneVolumeBooks();
    }

    @GetMapping(path = "getBooksInSection/{section}")
    public List<BookDTO> getBooksInSection(@PathVariable String section) {
        List<BookDTO> booksInThisSectionDTOS = new LinkedList<>();
        return booksInThisSectionDTOS = bookService.getBooksInSection(section);
    }

    @GetMapping(path = "getBooksBorrowedTo/{name}")
    public List<BookDTO> getBooksBorrowedTo(@PathVariable String name) {
        List<BookDTO> booksBorrowedToThisPersonDTOS = new LinkedList<>();
        return booksBorrowedToThisPersonDTOS = bookService.getBooksBorrowedTo(name);
    }

    @GetMapping(path = "getUnborrowedBooksInSection/{section}")
    public List<BookDTO> getUnborrowedBooksInSection(@PathVariable String section) {
        List<BookDTO> unborrowedBooksInSectionDTOS = new LinkedList<>();
        return unborrowedBooksInSectionDTOS = bookService.getUnborrowedBooksInThisSection(section);
    }

    @GetMapping(path = "getBorrowedBooksOfAuthor/{author}")
    public List<BookDTO> getBorrowedBooksOfAuthor(@PathVariable String author) {
        List<BookDTO> borrowedBooksOfAuthor = new LinkedList<>();
        return borrowedBooksOfAuthor = bookService.getBorrowedBooksOfAuthor(author);
    }

    @GetMapping(path = "orderBooksAscending")
    public List<BookDTO> orderBooksAscending(){
        List<BookDTO> ascendingListOfBooksDTOS = new LinkedList<>();
        return ascendingListOfBooksDTOS = bookService.getOrderListAscendant();
    }

    @DeleteMapping(path = "deleteBooksByTitleAuthorVolume/{title}/{author}/{volume}")
    public ResponseEntity deleteBooksbyTitleAuthorVolume(@PathVariable String title, @PathVariable String author, @PathVariable int volume){

        int rowsDeleted =bookService.deleteBooksByTitleAuthorVolume(title, author, volume);
        return ResponseEntity.ok(rowsDeleted + " books with title "+ title + " author "+author+" volume "+ volume + " deleted");
    }
}
